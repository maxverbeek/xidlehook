//! A module to check /proc/modules to see if the webcam module
//! (uvcvideo) is being used. If it is (reference count > 0)
//! then it prevents timers from being invoked.
use crate::{Module, Progress, Result, TimerInfo};

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

const PROC_MODULES_PATH: &str = "/proc/modules";
const WEBCAMMODULE: &str = "uvcvideo";

/// Subset representation of a kernel module
#[derive(Debug, PartialEq)]
struct KernelModule {
    name: String,
    refcount: i32,
}

/// Get a list of KernelModules as given by /proc/modules (and lsmod)
fn get_modules() -> Result<Vec<KernelModule>> {
    let modfile = File::open(PROC_MODULES_PATH)?;
    let lines = BufReader::new(modfile).lines();

    let mut modules = Vec::new();

    for line in lines {
        if let Ok(kmstring) = line {
            if let Some(m) = parse_module(&kmstring) {
                modules.push(m);
            }
        }
    }

    Ok(modules)
}

/// Parse a kernel module as given by info from /proc/modules
fn parse_module(kmstring: &String) -> Option<KernelModule> {
    let mut attrs = kmstring.trim().split(" ");

    let name = attrs.next()?.into();
    attrs.next()?; // mem size
    let refcount = attrs.next()?.parse().unwrap();

    Some(KernelModule { name, refcount })
}

/// Filters a list of kernel modules to find the one for the webcam
fn find_uvc_module(modules: Vec<KernelModule>) -> Option<KernelModule> {
    for module in modules {
        if module.name == WEBCAMMODULE {
            return Some(module);
        }
    }

    None
}

/// See the module-level documentation
#[derive(Clone, Copy, Debug)]
pub struct UvcKernelModule {}

impl UvcKernelModule {
    /// Create a new instance of this module
    pub fn new() -> Self {
        Self {}
    }
}

impl Module for UvcKernelModule {
    fn pre_timer(&mut self, _timer: TimerInfo) -> Result<Progress> {
        // query /proc/modules and find if the webcam module is loaded
        // and used. if so, abort the timer
        let modules = get_modules()?;

        if let Some(uvc) = find_uvc_module(modules) {
            if uvc.refcount > 0 {
                return Ok(Progress::Abort);
            }
        }

        Ok(Progress::Continue)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_generic_module() {
        // copy pasted from `cat /proc/modules`
        let modulestring =
            "ip_tables 28672 4 iptable_nat,iptable_raw,iptable_filter, Live 0x0000000000000000"
                .to_string();
        let km = parse_module(&modulestring);

        assert_eq!(Some(KernelModule { name: "ip_tables".into(), refcount: 4 }), km);
    }

    #[test]
    fn test_parse_uvcvideo_module() {
        let modulestring = "uvcvideo 110592 1 - Live 0x0000000000000000".to_string();

        let km = parse_module(&modulestring);

        assert_eq!(Some(KernelModule { name: "uvcvideo".into(), refcount: 1 }), km);
    }

    #[test]
    fn test_find_modules() {
        // empty line left in there on purpose
        let proc_module_contents = r##"
            rtw88_pci 28672 1 rtw88_8822be, Live 0x0000000000000000
            rtw88_core 147456 2 rtw88_8822b,rtw88_pci, Live 0x0000000000000000
            uvcvideo 110592 1 - Live 0x0000000000000000
            nls_iso8859_1 16384 1 - Live 0x0000000000000000
            snd_hda_codec_conexant 24576 1 - Live 0x0000000000000000

        "##.to_string();

        let modules = proc_module_contents
            .lines()
            .map(|s| s.to_string())
            .filter_map(|s| parse_module(&s))
            .collect();

        let uvc = find_uvc_module(modules);

        assert_eq!(Some(KernelModule { name: "uvcvideo".into(), refcount: 1 }), uvc);
    }

    #[test]
    fn test_parse_negative_refcount() {
        let modulestring = "uvcvideo 110592 -2 - Live 0x0000000000000000".to_string();

        let km = parse_module(&modulestring);

        assert_eq!(Some(KernelModule { name: "uvcvideo".into(), refcount: -2 }), km);
    }
}
